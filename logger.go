package logger

import (
	"context"
	"io"
)

type Fields map[string]interface{}

const (
	// Fields
	ErrorField = "error"
	TitleField = "title"

	// Outputs
	Stdout = "stdout"
	Stderr = "stderr"
	Vacuum = "vacuum"

	// Formatters
	JSONFormatter = "json"
	TextFormatter = "text"
)

type Logger interface {
	// Level calls
	Trace(args ...interface{})
	Debug(args ...interface{})
	Info(args ...interface{})
	Warning(args ...interface{})
	Error(args ...interface{})

	// Levelf calls
	Tracef(format string, args ...interface{})
	Debugf(format string, args ...interface{})
	Infof(format string, args ...interface{})
	Warningf(format string, args ...interface{})
	Errorf(format string, args ...interface{})

	// Levelln calls
	Traceln(args ...interface{})
	Debugln(args ...interface{})
	Infoln(args ...interface{})
	Warningln(args ...interface{})
	Errorln(args ...interface{})

	// StdLogger calls
	Print(args ...interface{})
	Printf(format string, args ...interface{})
	Println(args ...interface{})

	Fatal(args ...interface{})
	Fatalf(format string, args ...interface{})
	Fatalln(args ...interface{})

	Panic(args ...interface{})
	Panicf(format string, args ...interface{})
	Panicln(args ...interface{})

	// Native log with level
	Log(level Level, args ...interface{})
	Logf(level Level, format string, args ...interface{})
	Logln(level Level, args ...interface{})

	// Writer
	Writer() *io.PipeWriter

	// Context
	WithField(key string, value interface{}) Logger
	WithFields(fields Fields) Logger
	WithError(err error) Logger
}

var loggerKey struct{}

func WithLogger(ctx context.Context, logger Logger) context.Context {
	return context.WithValue(ctx, loggerKey, logger)
}

var DefaultLogger Logger = &logrusWrapper{}

func GetLogger(ctx context.Context) Logger {
	return getDefaultLogger(ctx, DefaultLogger)
}

func getDefaultLogger(ctx context.Context, defLogger Logger) Logger {
	l, ok := ctx.Value(loggerKey).(Logger)
	if !ok {
		l = defLogger
	}
	return l
}
