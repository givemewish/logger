package logger

import (
	"errors"
	"io"
	"os"
	"sync"
	"sync/atomic"

	"github.com/sirupsen/logrus"
)

type logrusWrapper struct {
	*logrus.Entry
	done uint32
	m    sync.Mutex
}

func (w *logrusWrapper) Do(f func()) {
	if atomic.LoadUint32(&w.done) == 0 {
		w.doSlow(f)
	}
}

func (w *logrusWrapper) doSlow(f func()) {
	w.m.Lock()
	defer w.m.Unlock()
	if w.done == 0 {
		defer atomic.StoreUint32(&w.done, 1)
		f()
	}
}

func (w *logrusWrapper) initDefault() {
	if w.Entry == nil {
		w.Entry = &logrus.Entry{
			Logger: &logrus.Logger{
				Out:          os.Stdout,
				Hooks:        make(logrus.LevelHooks),
				Formatter:    new(logrus.TextFormatter),
				ReportCaller: false,
				Level:        logrus.InfoLevel,
				ExitFunc:     os.Exit,
			},
			Data: make(logrus.Fields, 6),
		}
	}
}

func (w *logrusWrapper) Writer() *io.PipeWriter {
	w.Do(w.initDefault)
	return w.Entry.Writer()
}

func (w *logrusWrapper) Log(level Level, args ...interface{}) {
	w.Do(w.initDefault)
	w.Entry.Log(levelMap[level], args...)
}

func (w *logrusWrapper) Logf(level Level, format string, args ...interface{}) {
	w.Do(w.initDefault)
	w.Entry.Logf(levelMap[level], format, args...)
}

func (w *logrusWrapper) Logln(level Level, args ...interface{}) {
	w.Do(w.initDefault)
	w.Entry.Logln(levelMap[level], args...)
}

func (w *logrusWrapper) WithField(key string, value interface{}) Logger {
	w.Do(w.initDefault)
	return &logrusWrapper{done: w.done, Entry: w.Entry.WithField(key, value)}
}

func (w *logrusWrapper) WithFields(fields Fields) Logger {
	w.Do(w.initDefault)
	logrusFields := logrus.Fields(fields)
	return &logrusWrapper{done: w.done, Entry: w.Entry.WithFields(logrusFields)}
}

func (w *logrusWrapper) WithError(err error) Logger {
	w.Do(w.initDefault)
	return w.WithFields(Fields{ErrorField: err})
}

func WrapLogrusEntry(l *logrus.Entry) (Logger, error) {
	if l != nil {
		return &logrusWrapper{done: 1, Entry: l}, nil
	}
	return nil, errors.New("can't wrap nil")
}

func WrapLogrusLogger(l *logrus.Entry) (Logger, error) {
	if l != nil {
		return &logrusWrapper{done: 1, Entry: l.WithField("wrapp", "wrapped")}, nil
	}
	return nil, errors.New("can't wrap nil")
}
