package logger

import (
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
)

var formatters = map[string]logrus.Formatter{
	JSONFormatter: &logrus.JSONFormatter{},
	TextFormatter: &logrus.TextFormatter{},
}

func CreateLogger(config *Config) Logger {
	return initLogger(config)
}

func initLogger(config *Config) Logger {
	logger := logrus.New()

	switch config.Output {
	case Stdout, "":
		logger.SetOutput(os.Stdout)
	case Stderr:
		logger.SetOutput(os.Stderr)
	case Vacuum:
		logger.SetOutput(ioutil.Discard)
	default:
		f, err := os.OpenFile(config.Output, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
		if err != nil {
			logger.SetOutput(os.Stdout)
			logger.WithError(err).Error("failing to stdout")
		} else {
			logger.SetOutput(f)
		}
	}

	formatter, ok := formatters[config.Formatter]
	if !ok {
		formatter = &logrus.TextFormatter{}
	}
	logger.SetFormatter(formatter)
	logger.SetLevel(levelMap[config.Level])

	addHooks(logger, config)
	if config.Title != "" {
		return &logrusWrapper{done: 1, Entry: logger.WithField(TitleField, config.Title)}
	}
	return &logrusWrapper{done: 1, Entry: logger.WithFields(nil)}
}

func addHooks(logger *logrus.Logger, config *Config) {
	// todo: sentry hook
	// todo: syslog hook
	// todo: logstash hook
}
