package logger

type Config struct {
	Title     string `yaml:"title" json:"title" toml:"title"`
	Output    string `yaml:"output" json:"output" toml:"output"`
	Level     Level  `yaml:"level" json:"level" toml:"level"`
	Formatter string `yaml:"formatter" json:"formatter" toml:"formatter"`
}
